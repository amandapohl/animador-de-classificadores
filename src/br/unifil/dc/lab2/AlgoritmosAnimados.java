package br.unifil.dc.lab2;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Write a description of class AlgoritmosAnimados here.
 * 
 * @author Amanda Pohl & Sérgio Sarui
 * @version 26/04/2020
 */
public class AlgoritmosAnimados {
    public static Gravador listaEstatica(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Valores da lista imutável");
        return anim;
    }

    public static Gravador pesquisaSequencial(List<Integer> valores, int chave) {
        Gravador anim = new Gravador();                                           // Cria um novo objeto Gravador, da classe Gravador.
        anim.gravarLista(valores, "Inicio de pesquisa sequencial");         // Chama o método gravarLista da classe Gravador, para definir os valores da lista
        // conforme a lista de entrada no método pesquisaSequencial, e definir o nome da lista.

        int i = 0;                                                                // Define o valor 0 para a nova variável 'i'.
        anim.gravarIndiceDestacado(valores, i, "Pesquisa sequencial");      // Chama o método gravarIndiceDestacado da classe Gravador, para definir 'i' com a cor
        // amarela, criar um novo objeto ListaGravada (da classe ListaGravada), e adicioná-la na
        // lista 'Transparência'.
        while (i < valores.size() && valores.get(i) != chave) {                   // Enquanto o valor de i for menor que o tamanho da lista de entrada, e o valor na posição
            // de i na lista de entrada for diferente da chave de entrada, irá:
            i++;                                                                  // Somar 1 no valor de i.
            anim.gravarIndiceDestacado(valores, i, "Pesquisa sequencial");  // Chamar o método gravarIndiceDestacado novamente.
        }

        if (i < valores.size()) {                                                 // Se o valor de i for menor que o tamanho da lista de entrada, irá:
            anim.gravarIndiceDestacado(valores, i, "Chave encontrada");     // Chamar o método gravarIndiceDestacado novamente e informar que a chave foi encontrada.
        } else {                                                                  // Senão (se o valor de i for maior que o tamanho da lista), irá:
            anim.gravarLista(valores, "Chave não encontrada");              // Chamar o método gravarLista novamente, e informar que a chave não foi encontrada.
        }

        return anim;                                                              // Retorna o objetio 'anim', com a chave encontrada ou não.
    }


    /**
     * Método que executa a pesquisa binária na lista passada como parâmetro.
     * Mostra as iterações de forma ilustrativa, destacando
     * os elementos que estão sendo comparados.
     *
     * @param valores lista que contém os valores que serão iterados.
     * @param chave   valor que será pesquisado
     * @return Objeto Gravador de acordo com o valores da lista e a chave.
     */
    public static Gravador pesquisaBinaria(List<Integer> valores, int chave) {
        Gravador anim = new Gravador();
        ordenarElementos(valores);
        anim.gravarLista(valores, "Disposição inicial");

        List<Integer> auxValores = valores;

        int acumulaMeio = 0;
        int destaqueInicial = 0;
        int destaqueFinal = valores.size() - 1;
        do {
            int meio = valores.size() / 2;
            anim.gravarComparacaoComposta(auxValores, destaqueInicial, destaqueFinal,
                    (meio + acumulaMeio));
            if (valores.get(meio) == chave) {
                anim.gravarIndiceDestacado(auxValores, (meio + acumulaMeio),
                        "Chave encontrada");
                return anim;
            } else if (chave < valores.get(meio)) {
                valores = valores.subList(0, meio);
                destaqueFinal = meio;
            } else if (chave > valores.get(meio)) {
                acumulaMeio += meio + 1;
                valores = valores.subList(meio + 1, valores.size());
                destaqueInicial = acumulaMeio;
            }
        } while (valores.size() > 0);
        anim.gravarLista(auxValores, "Chave não encontrada");

        return anim;
    }

    /**
     * Ordena uma lista em ordem crescente pelo método "Bubble Sort".
     *
     * @param valores - Lista a ser ordenada
     * @return - Lista ordenada
     */
    public static Gravador classificarPorBolha(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição inicial");
        boolean houvePermuta;

        do {
            houvePermuta = false;
            // Sobe a bolha
            for (int i = 1; i < valores.size(); i++) {
//                anim.gravarComparacaoSimples(valores, i, i+1);
                if (valores.get(i - 1) > valores.get(i)) {
                    anim.gravarIndiceDestacado(valores, i, "Atual");
                    anim.gravarComparacaoSimples(valores, i - 1, i);
                    permutar(valores, i - 1, i);
                    houvePermuta = true;
//                    anim.gravarPosTrocas(valores, i, i+1);
                }
            }
        } while (houvePermuta);
        anim.gravarLista(valores, "Disposição final");
        System.out.println(valores);
        return anim;
    }

    /**
     * Ordena uma lista em ordem crescente pelo método "Selection Sort".
     *
     * @param valores - Lista a ser ordenada
     * @return - Lista ordenada
     */
    public static Gravador classificarPorSelecao(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição inicial");

        for (int i = 0; i < valores.size(); i++) {
            int menorIdx = encontrarIndiceMenorElem(valores, i);
            anim.gravarIndiceDestacado(valores, i, "Atual");
            anim.gravarComparacaoSimples(valores, menorIdx, i);
            permutar(valores, menorIdx, i);
            anim.gravarPosTrocas(valores, menorIdx, i);
        }

        anim.gravarLista(valores, "Disposição final");
        System.out.println(valores);
        return anim;
    }

    /**
     * Ordena uma lista em ordem crescente pelo método "Insertion Sort".
     *
     * @param valores - Lista a ser ordenada
     * @return - Lista ordenada
     */
    public static Gravador classificarPorInsercao(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição inicial");

        for (int i = 1; i < valores.size(); i++) {
            Integer elem = valores.get(i);
            int j = i;

//            anim.gravarComparacaoSimples(valores, j-1, elem);
            while (j > 0 && valores.get(j - 1) > elem) {
                anim.gravarIndiceDestacado(valores, i, "Atual");
                anim.gravarComparacaoSimples(valores, j, j - 1);
                valores.set(j, valores.get(j - 1)); // Deslocamento
                j--;
            }

            valores.set(j, elem);
//            anim.gravarPosTrocas(valores, j, elem);
        }
        anim.gravarLista(valores, "Disposição final");
        System.out.println(valores);
        return anim;
    }

    /**
     * Ordena uma lista em ordem crescente pelo método "Merge Sort".
     *
     * @param valores - Lista a ser ordenada
     * @return - Lista ordenada
     */
    public static Gravador classificarPorMerge(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição inicial");

        List<Integer> esquerda = new ArrayList<Integer>();
        List<Integer> direita = new ArrayList<Integer>();
        int centro;

        if (valores.size() == 1) {
            return anim;
        } else {
            centro = valores.size()/2;
            // copia a metade da esquerda.
            for (int i=0; i<centro; i++) {
                esquerda.add(valores.get(i));
            }

            // copia a metade da direita.
            for (int i=centro; i<valores.size(); i++) {
                direita.add(valores.get(i));
            }

            // Ordena a esquerda e direita.
            anim.gravarIndiceDestacado(valores, centro, "Atual");
            anim.gravarComparacaoSimples(valores, centro-1, centro);
            classificarPorMerge(esquerda);
            classificarPorMerge(direita);

            // realiza o merge.
            merge(valores, esquerda, direita, anim);
        }
        anim.gravarLista(valores, "Disposição final");
        System.out.println(valores);
        return anim;
    }


    /**
     * Ordena uma lista em ordem crescente pelo método "Quick Sort".
     *
     * @param valores - Lista a ser ordenada
     * @return - Lista ordenada
     */
    public static List<Integer> quickSort(List<Integer> valores, Gravador anim) {

        if (valores.isEmpty())
            return valores;
        else {
            int pivot = valores.get(0);

            List<Integer> less = new LinkedList<Integer>();
            List<Integer> pivotList = new LinkedList<Integer>();
            List<Integer> more = new LinkedList<Integer>();

            // Partition
            for (int i: valores) {
                if (i < pivot)
                    less.add(i);
                else if (i > pivot)
                    more.add(i);
                else
                    pivotList.add(i);
            }

            // Recursively sort sublists
            less = quickSort(less, anim);
            more = quickSort(more, anim);

            // Concatenate results
            less.addAll(pivotList);
            less.addAll(more);
            System.out.println(less);
           return less;
        }
    }
    public static Gravador classificarPorQuick(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição inicial");
        anim.gravarLista(quickSort(valores, anim), "Disposição final");
//            System.out.println(valores);
        return anim;
    }

    /**
     * Permuta (swap) dois elementos da lista de posição.
     * @param lista Lista cujos elementos serão permutados.
     * @param a Îndice do primeiro elemento a ser permutado.
     * @param b Îndice do outro elemento a ser permutado.
     */
    private static void permutar(List<Integer> lista, int a, int b) {
        Integer permutador = lista.get(a); // permutador = lista[a]
        lista.set(a, lista.get(b)); // lista[a] = lista[b]
        lista.set(b, permutador); // lista[b] = permutador
    }

    public static void merge(List<Integer> lista, List<Integer> esquerda, List<Integer> direita, Gravador anim) {
        int topoEsquerdo = 0;
        int topoDireito = 0;
        int topoAuxiliar = 0;
        while (topoEsquerdo < esquerda.size() && topoDireito < direita.size()) {
            if (esquerda.get(topoEsquerdo) < direita.get(topoDireito)) {
                anim.gravarIndiceDestacado(lista, topoAuxiliar, "Atual");
                anim.gravarComparacaoSimples(lista, topoAuxiliar, topoEsquerdo);
                lista.set(topoAuxiliar, esquerda.get(topoEsquerdo));
                topoEsquerdo++;
            } else {
                anim.gravarIndiceDestacado(lista, topoAuxiliar, "Atual");
                anim.gravarComparacaoSimples(lista, topoAuxiliar, topoDireito);
                lista.set(topoAuxiliar, direita.get(topoDireito));
                topoDireito++;
            }
            topoAuxiliar++;
        }

        List<Integer> faltantes; int topoF;
        if (topoEsquerdo < esquerda.size()) {
            faltantes = esquerda;topoF = topoEsquerdo;
        } else {
            faltantes = direita;topoF = topoDireito;
        }

        while (topoF < faltantes.size()) {
            lista.set(topoAuxiliar, faltantes.get(topoF));
            topoAuxiliar++; topoF++;
        }
    }

    /**
     * Encontra o índice do menor elemento da lista.
     * @param lista lista a ser procurada.
     * @return índice do elemento, na escala iniciada em zero.
     */
    private static int encontrarIndiceMenorElem(List<Integer> lista, int idxInicio) {
        int menor = idxInicio;
        for (int i = idxInicio+1; i < lista.size(); i++) {
            if (lista.get(menor) > lista.get(i))
                menor = i;
        }
        return menor;
    }
    /**
     * Ordena os elementos de qualquer lista utilizando o método
     * de ordenação por seleção.
     * @param valores Lista de valores a serem ordenados
     */
    public static void ordenarElementos(List<Integer> valores) {
        for (int i = 0; i < valores.size(); i++) {
            int menorIdx = encontrarIndiceMenorElem(valores, i);
            permutar(valores, menorIdx, i);
        }
    }
}