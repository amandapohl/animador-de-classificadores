package br.unifil.dc.lab2;

import java.util.List;
import java.util.ListIterator;
import java.util.ArrayList;
import java.awt.Color;

/**
 * Write a description of class Gravador here.
 *
 * @author Amanda Pohl & Sérgio Sarui
 * @version 26/04/2020
 */
public class Gravador
{
    /**
     * Inicia a lista que será utilizado para gravar as alterações realizas.
     */
    public Gravador() {
        this.seqGravacoes = new ArrayList<Transparencia>();
    }

    
    /** 
     * Grava a lista inicialmente com a cor BLUE
     * @param lista Lista de Cores, que contem a cor BLUE
     * @param nome String que será utilizada em "AlgoritmosAnimados"
     */
    public void gravarLista(List<Integer> lista, String nome) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, nome);
        seqGravacoes.add(gravacao);
    }

    
    /** 
     * Grava e altera a cor para amarelo, caso encontre a chave que desejado.
     * @param lista Lista de Cores, que inicialmente contém a cor BLUE
     * @param i Inteiro que será utilizado para saber qual indice deve trocar de cor.
     * @param nome String que será utilizada em "AlgoritmosAnimados"
     */
    public void gravarIndiceDestacado(List<Integer> lista, int i, String nome) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(i, Color.YELLOW);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, nome);
        seqGravacoes.add(gravacao);
    }

    
    /** 
     * Realiza uma comparação entre dois elementos da lista.
     * @param lista Lista de Cores, que inicialmente contém a cor BLUE
     * @param i Inteiro que será utilizado para comparação.
     * @param j Inteiro que será utilizado para comparação.
     */
    public void gravarComparacaoSimples(List<Integer> lista, int i, int j) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(i, Color.GRAY);
        cores.set(j, Color.GRAY);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, "Comparação");
        seqGravacoes.add(gravacao);
    }


    /**
     * Destaca os indices que estão sendo comparados.
     * @param lista lista de elementos a serem modificados
     * @param i primeiro indice a ser destacado
     * @param j segundo indice a ser destacado
     * @param k terceiro indice a ser destacado
     */
    public void gravarComparacaoComposta(List<Integer> lista, int i, int j, int k) {
        List<Integer> copia = new ArrayList<Integer>(lista);
        List<Color> cores = novaListaColors(lista.size(),Color.BLUE);
        cores.set(i, Color.GRAY);
        cores.set(j, Color.GRAY);
        cores.set(k, Color.YELLOW);
        ListaGravada gravacao = new ListaGravada(copia, cores, "Comparação");
        seqGravacoes.add(gravacao);
    }

    
    /** 
     * Realiza a troca de cores do indices indicados. 
     * @param lista Lista de Cores, que inicialmente contém a cor BLUE
     * @param i Inteiro que será utilizado para saber qual indice deve trocar de cor.
     * @param j Inteiro que será utilizado para saber qual indice deve trocar de cor.
     */
    public void gravarPosTrocas(List<Integer> lista, int i, int j) {
        List<Color> cores = novaListaColors(lista.size(), Color.BLUE);
        cores.set(i, Color.YELLOW);
        cores.set(j, Color.YELLOW);
        ListaGravada gravacao = new ListaGravada(List.copyOf(lista), cores, "Pós-troca");
        seqGravacoes.add(gravacao);
    }

    
    /** 
     * Chama o método listIterator
     * @return - Lista seqGravacoes, com as iterações que foram feitas
     */
    public ListIterator<Transparencia> getFilme() {
        return seqGravacoes.listIterator();
    }

    
    /** 
     * Cria uma nova lista de cores
     * @param numElems - Quantidade de elementos da lista
     * @param cor - Nome da cor
     * @return - Lista de cores
     */
    private static List<Color> novaListaColors(int numElems, Color cor) {
        ArrayList<Color> lista = new ArrayList<>(numElems);
        for (; numElems > 0; numElems--) lista.add(cor);
        return lista;
    }

    private List<Transparencia> seqGravacoes;
}