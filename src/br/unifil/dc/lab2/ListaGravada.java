package br.unifil.dc.lab2;

import javax.swing.JPanel;
import java.awt.*;
import java.util.Arrays;
import java.util.List;

/**
 * Write a description of class ListaGravada here.
 * 
 * @author Amanda Pohl & Sérgio Sarui
 * @version 21/04/2020
 */
public class ListaGravada implements Transparencia
{
    private List<Integer> lista;
    private List<Color> coresIndices;
    private String nome;
    //private List<Color> coresIndices2 = Arrays.asList("BLUE","YELLOW","RED","BLACK","WHITE","GREEN","GRAY","CYAN");
    private List<Color> coresIndices2 = Arrays.asList(Color.BLUE,Color.YELLOW,Color.RED,Color.CYAN,Color.GREEN,Color.PINK,
    Color.GRAY,Color.BLACK);
    private List<Color> coresIndices3;
    

    /**
     * Constructor for objects of class ListaGravada
     */
    public ListaGravada(List<Integer> lista, List<Color> coresIndices, String nome) {
        this.lista = lista;
        this.nome = nome;
        this.coresIndices = coresIndices;
    }

    /**
     * Método que desenha um gráfico dos 5 primeiros números de uma lista
     * @param pincel
     * @param contexto
     */
//    public void pintar(Graphics2D pincel, JPanel contexto) {
//        pincel.drawRect(50,50,100,100);       //1. Desenho qualquer que figura na tela do programa
//                                               // após apertar o botão “Carregar”.

//        pincel.setStroke(new BasicStroke(3));

//        int maior = 0;
//        for (int i = 0; i < lista.size() && i < 5; i++) {
//            if (lista.get(i) > maior) maior = lista.get(i);
//        }

//        int x = 20;
//        for (int i = 0; i < lista.size() && i < 5; i++) {
//            int height = 300 * lista.get(i) / maior;
//            pincel.setColor(Color.CYAN);
//            pincel.fillRect(x, 400 - height, 72, height);
//            pincel.setColor(Color.BLACK);
//            pincel.drawRect(x, 400 - height, 72, height);
//            pincel.drawString(String.valueOf(lista.get(i)), x+30, 420);
//            x += 72 + 50;
//        }
//    }

    /**
     *  Método que desenha o gráfico de uma lista qualquer
     * @param pincel
     * @param contexto
     */
    // public void pintar(Graphics2D pincel, JPanel contexto) {
    //     Dimension dim = contexto.getSize();
    //     int tamLista = lista.size();
    //     int espaco = (dim.width/3) / (tamLista+1);
    //     int largura = (dim.width * 2) / 3 / tamLista;
    //     int espacoAltura = dim.height / 10;
    //     pincel.setColor(Color.LIGHT_GRAY);
    //     pincel.drawLine(0, dim.height/10,  dim.width, dim.height/10);
    //     pincel.drawLine(0, dim.height-espacoAltura, dim.width, dim.height-espacoAltura);
    //     pincel.setStroke(new BasicStroke(3));

    //     int maior = 0;
    //     for (int i = 0; i < lista.size(); i++) {
    //         if (lista.get(i) > maior) maior = lista.get(i);
    //     }

    //     int x = espaco;
    //     for (int i = 0; i < lista.size(); i++) {
    //         int height = ((dim.height - (espacoAltura*2)) * lista.get(i)) / maior;
    //         pincel.setColor(Color.CYAN);
    //         pincel.fillRect(x, (dim.height - espacoAltura) - height, largura, height);
    //         pincel.setColor(Color.GRAY);
    //         pincel.drawRect(x, (dim.height - espacoAltura) - height, largura, height);
    //         pincel.drawString(String.valueOf(lista.get(i)), x, (dim.height - espacoAltura) + 12);
    //         x += largura + espaco;
    //     }
    // }



    /**
     *  Método que desenha o gráfico de uma lista qualquer
     * @param pincel
     * @param contexto
     */
    public void pintar(Graphics2D pincel, JPanel contexto) {
        
        Dimension dim = contexto.getSize();
        int tamLista = lista.size();
        int espaco = (dim.width/3) / (tamLista+1);
        int largura = (dim.width * 2) / 3 / tamLista;
        int espacoAltura = dim.height / 10;
        pincel.setColor(Color.LIGHT_GRAY);
        pincel.drawLine(0, dim.height/10,  dim.width, dim.height/10);
        pincel.drawLine(0, dim.height-espacoAltura, dim.width, dim.height-espacoAltura);
        pincel.setStroke(new BasicStroke(3));

        int maior = 0;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) > maior) maior = lista.get(i);
        }

        int x = espaco;
        for (int i = 0; i < lista.size(); i++) {
            if(coresIndices == null){
                int height = ((dim.height - (espacoAltura*2)) * lista.get(i)) / maior;
                pincel.setColor(Color.CYAN); //CYAN
                pincel.fillRect(x, (dim.height - espacoAltura) - height, largura, height);
                pincel.setColor(Color.GRAY);
                pincel.drawRect(x, (dim.height - espacoAltura) - height, largura, height);
                pincel.drawString(String.valueOf(lista.get(i)), x, (dim.height - espacoAltura) + 12);
                x += largura + espaco;
            }else{
                int height = ((dim.height - (espacoAltura*2)) * lista.get(i)) / maior;
                pincel.setColor(coresIndices.get(i));
                pincel.fillRect(x, (dim.height - espacoAltura) - height, largura, height);
                pincel.setColor(Color.GRAY);
                pincel.drawRect(x, (dim.height - espacoAltura) - height, largura, height);
                pincel.drawString(String.valueOf(lista.get(i)), x, (dim.height - espacoAltura) + 12);
                x += largura + espaco;
                
            }
        }
    }
}
